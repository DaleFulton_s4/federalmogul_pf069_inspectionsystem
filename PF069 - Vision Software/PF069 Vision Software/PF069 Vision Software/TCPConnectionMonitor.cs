﻿using System;
using nsEventDistributer;
using nsEventTypes;

namespace Lib.TcpConnectionMonitor
{
    public class TcpConnectionMonitor
    {
        private bool isConnected = false;

        public string Address { get; } = "";


        // public event EventHandler<SendBoolEventArgs>ConnectionStatusChanged;
        public TcpConnectionMonitor(string ipAddress)
        {
            Address = ipAddress;
        }

     
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }
            set
            {
                if (value != isConnected)
                {
                    isConnected = value;
                    EventDistributer.Publish(new ConnectionStatusChangedEvt(IsConnected));    
                }
            }
        }
    }
}