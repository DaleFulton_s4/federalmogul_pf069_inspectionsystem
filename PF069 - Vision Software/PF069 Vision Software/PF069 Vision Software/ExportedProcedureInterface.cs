using HalconDotNet;
using HalconInterface;
using NLog;
using System;
using PF069_Vision_Software;

namespace Lib.HalconProcedureInterface
{
    static class HalconProcedureInterface
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static bool FG_Open(HTuple Interface, HTuple ColorSpace, HTuple CameraType, HTuple Device)
        {
            try
            {
                HDevProcedure Proc = new HDevProcedure(HalconInitialise.Program, "FG_Open");
                HDevProcedureCall ProcCall = new HDevProcedureCall(Proc);
                ProcCall.SetInputCtrlParamTuple("Interface", Interface);
                ProcCall.SetInputCtrlParamTuple("ColorSpace", ColorSpace);
                ProcCall.SetInputCtrlParamTuple("CameraType", CameraType);
                ProcCall.SetInputCtrlParamTuple("Device", Device);
                ProcCall.Execute();
                HTuple AcqHandle = ProcCall.GetOutputCtrlParamTuple("AcqHandle");
                ConfigGlobals._FrameGrabberList.Find(a => a._Device == Device)._AcqHandle = AcqHandle;

                return true;
            }
            catch (HDevEngineException Exception)
            {
                logger.Error(Exception);
                return false;
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
        }
        public static bool FG_Close(HTuple AcqHandle)
        {
            try
            {
                HDevProcedure Proc = new HDevProcedure(HalconInitialise.Program, "FG_Close");
                HDevProcedureCall ProcCall = new HDevProcedureCall(Proc);
                ProcCall.SetInputCtrlParamTuple("AcqHandle", AcqHandle);
                ProcCall.Execute();
                return true;
            }
            catch (HDevEngineException Exception)
            {
                logger.Error(Exception);
                return false;
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
        }
        public static bool Inspection1(HTuple NumberItems, HTuple AreaMin, HTuple AreaMax, HTuple AcqHandle, HTuple SaveImages, HTuple ImageName, out bool OverallResult, out bool Result1, out bool Result2, out bool Result3, out bool Result4, out bool Result5, out bool Result6)
        {
            try
            {            
                HDevProcedure Proc = new HDevProcedure(HalconInitialise.Program, "Inspection1");
                HDevProcedureCall ProcCall = new HDevProcedureCall(Proc);
                ProcCall.SetInputCtrlParamTuple("NumberItems", NumberItems);
                ProcCall.SetInputCtrlParamTuple("AreaMin", AreaMin);
                ProcCall.SetInputCtrlParamTuple("AreaMax", AreaMax);
                ProcCall.SetInputCtrlParamTuple("AcqHandle", AcqHandle);
                ProcCall.SetInputCtrlParamTuple("SaveImages",  SaveImages);
                ProcCall.SetInputCtrlParamTuple("ImageName",ImageName);
                ProcCall.Execute();
                OverallResult = ProcCall.GetOutputCtrlParamTuple("OverallResult");
                Result1 = ProcCall.GetOutputCtrlParamTuple("Result1");
                Result2 = ProcCall.GetOutputCtrlParamTuple("Result2");
                Result3 = ProcCall.GetOutputCtrlParamTuple("Result3");
                Result4 = ProcCall.GetOutputCtrlParamTuple("Result4");
                Result5 = ProcCall.GetOutputCtrlParamTuple("Result5");
                Result6 = ProcCall.GetOutputCtrlParamTuple("Result6");
                return true;
            }
            catch (HDevEngineException Exception)
            {
                logger.Error(Exception);
                OverallResult = false;
                Result1 = false;
                Result2 = false;
                Result3 = false;
                Result4 = false;
                Result5 = false;
                Result6 = false;
                return false;
            }
            catch (Exception e)
            {
                logger.Error(e);
                OverallResult = false;
                Result1 = false;
                Result2 = false;
                Result3 = false;
                Result4 = false;
                Result5 = false;
                Result6 = false;
                return false;
            }
        }

        public static bool Inspection2(HTuple AcqHandle, out bool OverallResult, out bool Result7, out bool Result8, out bool Result9,out bool Result10, out bool Result11)
        {
            try
            {
                HDevProcedure Proc = new HDevProcedure(HalconInitialise.Program, "Inspection2");
                HDevProcedureCall ProcCall = new HDevProcedureCall(Proc);
                ProcCall.SetInputCtrlParamTuple("AcqHandle", AcqHandle);
                ProcCall.Execute();
                OverallResult = ProcCall.GetOutputCtrlParamTuple("OverallResult");
                Result7 = ProcCall.GetOutputCtrlParamTuple("Result7");
                Result8 = ProcCall.GetOutputCtrlParamTuple("Result8");
                Result9 = ProcCall.GetOutputCtrlParamTuple("Result9");
                Result10 = ProcCall.GetOutputCtrlParamTuple("Result10");
                Result11 = ProcCall.GetOutputCtrlParamTuple("Result11");
                
                return true;
            }
            catch (HDevEngineException Exception)
            {
                logger.Error(Exception);
                OverallResult = false;
                Result7 = false;
                Result8 = false;
                Result9 = false;
                Result10 = false;
                Result11 = false;
                return false;
            }
            catch (Exception e)
            {
                logger.Error(e);
                OverallResult = false;
                Result7 = false;
                Result8 = false;
                Result9 = false;
                Result10 = false;
                Result11 = false;
                return false;
            }
        }

        public static bool Inspection3(HTuple AcqHandle, out bool OverallResult, out bool Result12)
        {
            try
            {


                HDevProcedure Proc = new HDevProcedure(HalconInitialise.Program, "Inspection3");
                HDevProcedureCall ProcCall = new HDevProcedureCall(Proc);
                ProcCall.SetInputCtrlParamTuple("AcqHandle", AcqHandle);
                ProcCall.Execute();
                OverallResult = ProcCall.GetOutputCtrlParamTuple("OverallResult");
                Result12 = ProcCall.GetOutputCtrlParamTuple("Result12");

                return true;
            }
            catch (HDevEngineException Exception)
            {
                logger.Error(Exception);
                OverallResult = false;
                Result12 = false;
                return false;
            }
            catch (Exception e)
            {
                logger.Error(e);
                OverallResult = false;
                Result12 = false;
                return false;
            }
        }
    }
}

