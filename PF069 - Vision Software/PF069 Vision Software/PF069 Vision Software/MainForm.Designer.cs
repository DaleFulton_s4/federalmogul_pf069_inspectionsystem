﻿namespace PF069_Vision_Software
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.xtraTabControlSecondary = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.tableLayoutPanelCurrentInspection = new System.Windows.Forms.TableLayoutPanel();
            this.lblVisionStatus = new System.Windows.Forms.Label();
            this.lblProgram = new System.Windows.Forms.Label();
            this.lblCurrentInspection = new System.Windows.Forms.Label();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.tableLayoutPanelStatus = new System.Windows.Forms.TableLayoutPanel();
            this.rtbLogs = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pnlPLCStatus = new System.Windows.Forms.Panel();
            this.pnlDatabase = new System.Windows.Forms.Panel();
            this.lblPLC = new System.Windows.Forms.Label();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.tableLayoutPanelCurrent = new System.Windows.Forms.TableLayoutPanel();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.xtraTabControlMain = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.halconDisplayControl1 = new lib.HalconUserControl.HalconDisplayControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.halconDisplayControl2 = new lib.HalconUserControl.HalconDisplayControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.halconDisplayControl3 = new lib.HalconUserControl.HalconDisplayControl();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlSecondary)).BeginInit();
            this.xtraTabControlSecondary.SuspendLayout();
            this.xtraTabPage5.SuspendLayout();
            this.tableLayoutPanelCurrentInspection.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            this.tableLayoutPanelStatus.SuspendLayout();
            this.tableLayoutPanelCurrent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlMain)).BeginInit();
            this.xtraTabControlMain.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "ribbonPage3";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "ribbonPageGroup3";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "ribbonPage4";
            // 
            // xtraTabControlSecondary
            // 
            this.xtraTabControlSecondary.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraTabControlSecondary.Appearance.Options.UseBackColor = true;
            this.xtraTabControlSecondary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlSecondary.Location = new System.Drawing.Point(1189, 101);
            this.xtraTabControlSecondary.Name = "xtraTabControlSecondary";
            this.xtraTabControlSecondary.SelectedTabPage = this.xtraTabPage5;
            this.xtraTabControlSecondary.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabControlSecondary.Size = new System.Drawing.Size(390, 827);
            this.xtraTabControlSecondary.TabIndex = 7;
            this.xtraTabControlSecondary.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.tableLayoutPanelCurrentInspection);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(383, 820);
            this.xtraTabPage5.Text = "xtraTabPage5";
            // 
            // tableLayoutPanelCurrentInspection
            // 
            this.tableLayoutPanelCurrentInspection.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelCurrentInspection.ColumnCount = 1;
            this.tableLayoutPanelCurrentInspection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCurrentInspection.Controls.Add(this.lblVisionStatus, 0, 4);
            this.tableLayoutPanelCurrentInspection.Controls.Add(this.lblProgram, 0, 2);
            this.tableLayoutPanelCurrentInspection.Controls.Add(this.lblCurrentInspection, 0, 1);
            this.tableLayoutPanelCurrentInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCurrentInspection.ForeColor = System.Drawing.Color.White;
            this.tableLayoutPanelCurrentInspection.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelCurrentInspection.Name = "tableLayoutPanelCurrentInspection";
            this.tableLayoutPanelCurrentInspection.RowCount = 5;
            this.tableLayoutPanelCurrentInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelCurrentInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelCurrentInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelCurrentInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelCurrentInspection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelCurrentInspection.Size = new System.Drawing.Size(383, 820);
            this.tableLayoutPanelCurrentInspection.TabIndex = 0;
            // 
            // lblVisionStatus
            // 
            this.lblVisionStatus.AutoSize = true;
            this.lblVisionStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblVisionStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVisionStatus.Font = new System.Drawing.Font("Impact", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisionStatus.ForeColor = System.Drawing.Color.White;
            this.lblVisionStatus.Location = new System.Drawing.Point(3, 164);
            this.lblVisionStatus.Name = "lblVisionStatus";
            this.lblVisionStatus.Size = new System.Drawing.Size(377, 656);
            this.lblVisionStatus.TabIndex = 20;
            this.lblVisionStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblVisionStatus.Click += new System.EventHandler(this.lblResult_Click);
            // 
            // lblProgram
            // 
            this.lblProgram.AutoSize = true;
            this.lblProgram.BackColor = System.Drawing.Color.Transparent;
            this.lblProgram.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProgram.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblProgram.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProgram.Location = new System.Drawing.Point(3, 82);
            this.lblProgram.Name = "lblProgram";
            this.lblProgram.Size = new System.Drawing.Size(377, 41);
            this.lblProgram.TabIndex = 16;
            this.lblProgram.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurrentInspection
            // 
            this.lblCurrentInspection.AutoSize = true;
            this.lblCurrentInspection.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrentInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCurrentInspection.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblCurrentInspection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCurrentInspection.Location = new System.Drawing.Point(3, 41);
            this.lblCurrentInspection.Name = "lblCurrentInspection";
            this.lblCurrentInspection.Size = new System.Drawing.Size(377, 41);
            this.lblCurrentInspection.TabIndex = 10;
            this.lblCurrentInspection.Text = "Current Inspection:";
            this.lblCurrentInspection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCurrentInspection.Click += new System.EventHandler(this.lblCurrentInspection_Click);
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Appearance.PageClient.BackColor = System.Drawing.Color.DimGray;
            this.xtraTabPage6.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage6.Controls.Add(this.tableLayoutPanelStatus);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(383, 820);
            this.xtraTabPage6.Text = "xtraTabPage6";
            // 
            // tableLayoutPanelStatus
            // 
            this.tableLayoutPanelStatus.ColumnCount = 1;
            this.tableLayoutPanelStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelStatus.Controls.Add(this.rtbLogs, 0, 6);
            this.tableLayoutPanelStatus.Controls.Add(this.lblStatus, 0, 0);
            this.tableLayoutPanelStatus.Controls.Add(this.pnlPLCStatus, 0, 2);
            this.tableLayoutPanelStatus.Controls.Add(this.pnlDatabase, 0, 4);
            this.tableLayoutPanelStatus.Controls.Add(this.lblPLC, 0, 1);
            this.tableLayoutPanelStatus.Controls.Add(this.lblDatabase, 0, 3);
            this.tableLayoutPanelStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelStatus.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelStatus.Name = "tableLayoutPanelStatus";
            this.tableLayoutPanelStatus.RowCount = 7;
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanelStatus.Size = new System.Drawing.Size(383, 820);
            this.tableLayoutPanelStatus.TabIndex = 1;
            // 
            // rtbLogs
            // 
            this.rtbLogs.BackColor = System.Drawing.Color.AliceBlue;
            this.rtbLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogs.Location = new System.Drawing.Point(3, 249);
            this.rtbLogs.Name = "rtbLogs";
            this.rtbLogs.Size = new System.Drawing.Size(377, 568);
            this.rtbLogs.TabIndex = 11;
            this.rtbLogs.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblStatus.Location = new System.Drawing.Point(3, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(377, 41);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "Connection Status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPLCStatus
            // 
            this.pnlPLCStatus.BackColor = System.Drawing.Color.DarkRed;
            this.pnlPLCStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPLCStatus.Location = new System.Drawing.Point(3, 85);
            this.pnlPLCStatus.Name = "pnlPLCStatus";
            this.pnlPLCStatus.Size = new System.Drawing.Size(377, 35);
            this.pnlPLCStatus.TabIndex = 1;
            // 
            // pnlDatabase
            // 
            this.pnlDatabase.BackColor = System.Drawing.Color.Maroon;
            this.pnlDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDatabase.Location = new System.Drawing.Point(3, 167);
            this.pnlDatabase.Name = "pnlDatabase";
            this.pnlDatabase.Size = new System.Drawing.Size(377, 35);
            this.pnlDatabase.TabIndex = 6;
            // 
            // lblPLC
            // 
            this.lblPLC.AutoSize = true;
            this.lblPLC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPLC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPLC.Location = new System.Drawing.Point(3, 41);
            this.lblPLC.Name = "lblPLC";
            this.lblPLC.Size = new System.Drawing.Size(377, 41);
            this.lblPLC.TabIndex = 7;
            this.lblPLC.Text = "PLC";
            this.lblPLC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDatabase.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.lblDatabase.Location = new System.Drawing.Point(3, 123);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(377, 41);
            this.lblDatabase.TabIndex = 8;
            this.lblDatabase.Text = "DATABASE";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDatabase.Click += new System.EventHandler(this.lblCamera_Click);
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeading.Font = new System.Drawing.Font("Impact", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblHeading.Location = new System.Drawing.Point(3, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(1180, 98);
            this.lblHeading.TabIndex = 6;
            this.lblHeading.Text = "HEAT SHIELD INSPECTION";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHeading.Click += new System.EventHandler(this.lblHeading_Click);
            // 
            // tableLayoutPanelCurrent
            // 
            this.tableLayoutPanelCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanelCurrent.ColumnCount = 2;
            this.tableLayoutPanelCurrent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanelCurrent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelCurrent.Controls.Add(this.lblHeading, 0, 0);
            this.tableLayoutPanelCurrent.Controls.Add(this.windowsUIButtonPanel1, 1, 0);
            this.tableLayoutPanelCurrent.Controls.Add(this.xtraTabControlMain, 0, 1);
            this.tableLayoutPanelCurrent.Controls.Add(this.statusStrip1, 0, 2);
            this.tableLayoutPanelCurrent.Controls.Add(this.xtraTabControlSecondary, 1, 1);
            this.tableLayoutPanelCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCurrent.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelCurrent.Name = "tableLayoutPanelCurrent";
            this.tableLayoutPanelCurrent.RowCount = 3;
            this.tableLayoutPanelCurrent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelCurrent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanelCurrent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelCurrent.Size = new System.Drawing.Size(1582, 981);
            this.tableLayoutPanelCurrent.TabIndex = 3;
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.BorderColor = System.Drawing.SystemColors.Window;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.Silver;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseBorderColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.BackColor = System.Drawing.Color.SkyBlue;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.BorderColor = System.Drawing.Color.Transparent;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.Silver;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseBorderColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Results", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Parameters", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Status", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.CheckButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Save Images", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.CheckButton, "", -1, true, null, true, true, true, null, -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.windowsUIButtonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowsUIButtonPanel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(1189, 3);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(390, 92);
            this.windowsUIButtonPanel1.TabIndex = 2;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            this.windowsUIButtonPanel1.WrapButtons = true;
            this.windowsUIButtonPanel1.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanel1_ButtonClick);
            this.windowsUIButtonPanel1.ButtonUnchecked += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanel1_ButtonUnchecked);
            this.windowsUIButtonPanel1.ButtonChecked += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanel1_ButtonChecked);
            this.windowsUIButtonPanel1.Click += new System.EventHandler(this.windowsUIButtonPanel1_Click);
            // 
            // xtraTabControlMain
            // 
            this.xtraTabControlMain.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraTabControlMain.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xtraTabControlMain.Appearance.Options.UseBackColor = true;
            this.xtraTabControlMain.Appearance.Options.UseForeColor = true;
            this.xtraTabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlMain.Location = new System.Drawing.Point(3, 101);
            this.xtraTabControlMain.Name = "xtraTabControlMain";
            this.xtraTabControlMain.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControlMain.Size = new System.Drawing.Size(1180, 827);
            this.xtraTabControlMain.TabIndex = 3;
            this.xtraTabControlMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            this.xtraTabControlMain.Click += new System.EventHandler(this.xtraTabControlMain_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.halconDisplayControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1173, 793);
            this.xtraTabPage1.Text = "INSPECTION 1";
            // 
            // halconDisplayControl1
            // 
            this.halconDisplayControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.halconDisplayControl1.Location = new System.Drawing.Point(0, 0);
            this.halconDisplayControl1.Name = "halconDisplayControl1";
            this.halconDisplayControl1.Size = new System.Drawing.Size(1173, 793);
            this.halconDisplayControl1.TabIndex = 0;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.halconDisplayControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1173, 793);
            this.xtraTabPage2.Text = "INSPECTION 2";
            // 
            // halconDisplayControl2
            // 
            this.halconDisplayControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.halconDisplayControl2.Location = new System.Drawing.Point(0, 0);
            this.halconDisplayControl2.Name = "halconDisplayControl2";
            this.halconDisplayControl2.Size = new System.Drawing.Size(1173, 793);
            this.halconDisplayControl2.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.halconDisplayControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1173, 793);
            this.xtraTabPage3.Text = "INSPECTION 3";
            // 
            // halconDisplayControl3
            // 
            this.halconDisplayControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.halconDisplayControl3.Location = new System.Drawing.Point(0, 0);
            this.halconDisplayControl3.Name = "halconDisplayControl3";
            this.halconDisplayControl3.Size = new System.Drawing.Size(1173, 793);
            this.halconDisplayControl3.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanelCurrent.SetColumnSpan(this.statusStrip1, 2);
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 931);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1582, 50);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel1
            // 
            this.StatusLabel1.Name = "StatusLabel1";
            this.StatusLabel1.Size = new System.Drawing.Size(151, 45);
            this.StatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1582, 981);
            this.Controls.Add(this.tableLayoutPanelCurrent);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1600, 1028);
            this.Name = "MainForm";
            this.Text = "Inpsection System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlSecondary)).EndInit();
            this.xtraTabControlSecondary.ResumeLayout(false);
            this.xtraTabPage5.ResumeLayout(false);
            this.tableLayoutPanelCurrentInspection.ResumeLayout(false);
            this.tableLayoutPanelCurrentInspection.PerformLayout();
            this.xtraTabPage6.ResumeLayout(false);
            this.tableLayoutPanelStatus.ResumeLayout(false);
            this.tableLayoutPanelStatus.PerformLayout();
            this.tableLayoutPanelCurrent.ResumeLayout(false);
            this.tableLayoutPanelCurrent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlMain)).EndInit();
            this.xtraTabControlMain.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlSecondary;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCurrent;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCurrentInspection;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel pnlPLCStatus;
        private System.Windows.Forms.Panel pnlDatabase;
        private System.Windows.Forms.Label lblPLC;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblProgram;
        private System.Windows.Forms.Label lblCurrentInspection;
        private System.Windows.Forms.RichTextBox rtbLogs;
        private System.Windows.Forms.Label lblVisionStatus;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlMain;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private lib.HalconUserControl.HalconDisplayControl halconDisplayControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private lib.HalconUserControl.HalconDisplayControl halconDisplayControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private lib.HalconUserControl.HalconDisplayControl halconDisplayControl3;
    }
}

