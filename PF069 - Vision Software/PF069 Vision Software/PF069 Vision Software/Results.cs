﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;

namespace PF069_Vision_Software
{
    public partial class frmResults : Form
    {
        
        private ModelDBContext _dbContext = null;

        // pass main form dbContext to be used here. For gridcontrol to display updated data, one instance of dbcontext needs to be created and bound
        // to the grid, before adding or updating the table.  Since updating of results occurs on main form, one instance of dbcontext is created on the main form and passed here to be bound
        // before updating results (on the main form).
        public frmResults(ModelDBContext dbContext)
        {
            InitializeComponent();
            this._dbContext = dbContext;
            this.MinimizeBox = false;


            dbContext.Results.LoadAsync().ContinueWith(loadTask =>
            {
                // Bind data to control when loading complete
                resultsBindingSource2.DataSource = dbContext.Results.Local.ToBindingList();
            }, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

            GridFormatRule gridFormatRule = new GridFormatRule();
            FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
            gridFormatRule.Column = colOverallResult;
            gridFormatRule.ApplyToRow = false;
            formatConditionRuleExpression.PredefinedName = "Green Fill";
            formatConditionRuleExpression.Expression = "[OverallResult] = 'PASS'";
            gridFormatRule.Rule = formatConditionRuleExpression;
            gridView1.FormatRules.Add(gridFormatRule);

            GridFormatRule gridFormatRule1 = new GridFormatRule();
            FormatConditionRuleExpression formatConditionRuleExpression1 = new FormatConditionRuleExpression();
            gridFormatRule1.Column = colOverallResult;
            gridFormatRule1.ApplyToRow = false;
            formatConditionRuleExpression1.PredefinedName = "Red Fill";
            formatConditionRuleExpression1.Expression = "[OverallResult] = 'FAIL'";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridView1.FormatRules.Add(gridFormatRule1);

            
            colTimeStamp.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:ss";


        }

        private void frmResults_Load(object sender, EventArgs e)
        {

        }

        private void gridControlResults_Click(object sender, EventArgs e)
        {

        }
    }
}
