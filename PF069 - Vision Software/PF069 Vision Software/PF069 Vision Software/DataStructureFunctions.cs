﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PF069_Vision_Software
{
    public static class DataStructureFunctions
    {
        // ensure bytes to be swapped are converted to a byte array
        public static byte[] SwapBytes(byte[] paBytes)
        {
            byte[] lobyte = paBytes;
            lobyte = lobyte.Reverse().ToArray();
            return lobyte;
        }

        // copy a segment of bytes to another byte array of the same size or smaller
        public static byte[] CopyByteArray(byte[] InputByteArray, int InputByteArrayIndex, int LengthToCopy, bool EndianSwap)
        {
            byte[] OutputArray = new byte[LengthToCopy];
            Array.Copy(InputByteArray, InputByteArrayIndex, OutputArray, 0, LengthToCopy);
            
            if (EndianSwap)
            {
                OutputArray = SwapBytes(OutputArray);
            }
            return OutputArray;     
        }
        // removes all spaces and special characters from a string
        public static string StringRemoveSpecialChar(string input)
        {
            StringBuilder SB = new StringBuilder(input.Length);

            foreach (char c in input)
            {
                if ((c >= 'a') && (c <= 'Z') || ((c >= '0') && (c <= '9')))
                {
                    SB.Append(c);
                }
            }

            return SB.ToString();
        }
    }
}
