﻿using NLog;
using System;
using System.Collections.Concurrent;

namespace Lib.TaskManager
{
    public sealed class TaskManager
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly Lazy<TaskManager> taskManager = new Lazy<TaskManager>(() => new TaskManager());

        private static ConcurrentQueue<BaseTask> tasks = new ConcurrentQueue<BaseTask>();
        private bool isBusy = false;

        private BaseTask currentTask = null;


        private TaskManager()
        {
        }

        public static TaskManager Instance
        {
            get { return taskManager.Value; }
        }

        public void AbortCurrentTask()
        {
            if (currentTask != null)
            { currentTask.Abort(); }
            
        }

        public void Clear()
        {      
            tasks = new ConcurrentQueue<BaseTask>();
            logger.Info(" Task Queue has been Cleared");
            AbortCurrentTask();
        }

        public void RunConcurrently(BaseTask task)
        {
            throw new NotImplementedException();
        }

        public void RunSequentially(BaseTask task)
        {
            if ((tasks.Count < 1) && !isBusy)
            {
                Run(task);
            }
            else
            {
                logger.Trace("Enqueue: Task" + task.ID);
                tasks.Enqueue(task);
            }
        }

        public void SubscribeAllOptions(ref BaseTask task)
        {
            task.TaskCompleted += Task_TaskCompleted;
            task.TaskAborted += Task_TaskCompleted;
            task.TaskFailed += Task_TaskCompleted;
     
        }

        private void UnSubscribeAllOptions(ref BaseTask task)
        {
            task.TaskCompleted -= Task_TaskCompleted;
            task.TaskAborted -= Task_TaskCompleted;
            task.TaskFailed -= Task_TaskCompleted;
        }


        private void Run(BaseTask task)
        {
            isBusy = true;
            SubscribeAllOptions(ref task);
            currentTask = task;
            task.Run();
        }


        private void Task_TaskCompleted(object sender, EventArgs e)
        {
            if ((sender is BaseTask task) /*&& task.Completed*/ )
            {
                isBusy = false;
                UnSubscribeAllOptions(ref task);
                if (tasks.Count > 0)
                {
                    while (tasks.TryDequeue(out BaseTask nextTask))
                    {
                        logger.Trace("Dequeue and run: Task" + nextTask.ID);
                        Run(nextTask);
                        break;
                    }
                }
                else
                {
                    logger.Trace("No pending tasks");
                }
            }
        }
    }
}