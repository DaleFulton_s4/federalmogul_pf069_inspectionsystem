﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;


namespace PF069_Vision_Software
{


    internal class DataStructure
    {
        public int _Header;
        public Int16 _Mode;
        public Int16 _Start;
        public Int16 _ProgramNo;
        public bool _FormatCorrect;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private byte[] _ReceivedBytes;


        public DataStructure(byte[] InputBytes)
        {
            _Header = -1;
            _Mode = -1;
            _Start = -1;
            _ProgramNo = -1;
            _FormatCorrect = false;
            _ReceivedBytes = new byte[InputBytes.Length];
            _ReceivedBytes = InputBytes;

            DecodeData();

        }

        

        public void DecodeData()
        {
            try
            {

                logger.Info("Received Bytes are: " + _ReceivedBytes);



                _Header = BitConverter.ToInt32(DataStructureFunctions.CopyByteArray(_ReceivedBytes, 0, 4, true), 0);
                logger.Trace("Header int value :" + _Header);

                // if header is what is expected, packet is correct
                //Testing, decimal value fo 65 is the Ascii char A
                if (_Header == 69)
                {
                    _Mode = BitConverter.ToInt16(DataStructureFunctions.CopyByteArray(_ReceivedBytes, 4, 2, true), 0);
                    _Start = BitConverter.ToInt16(DataStructureFunctions.CopyByteArray(_ReceivedBytes, 6, 2, true), 0);
                    _ProgramNo = BitConverter.ToInt16(DataStructureFunctions.CopyByteArray(_ReceivedBytes, 8, 2, true), 0);
                    _FormatCorrect = true;

                    logger.Info("Data Received Mode : {0}", _Mode.ToString());
                    logger.Info("Data Received Start/Abort: {0}", _Start.ToString());
                    logger.Info("Data Received Program Number: {0}", _ProgramNo.ToString());

                }
                else
                {
                    logger.Error("COMMUNICATION ERROR: Data received contains an incorrect static header: {0}", _Header.ToString());
                    _FormatCorrect = false;

                }

                //publish event back to UI to indicate whether testing is going to perform or if coms failed.
            }
            catch (Exception e)
            {
                logger.Error(e);
                _FormatCorrect = false;
            }
        }

    }

    // string data structure for sending results to a database.
    internal class DBResultData
    {
        // dictionary contains all results as well as variant id.
        public Dictionary<string, string> DBResults;
        //datetime not included in dictionary is it must remain DateTime, not be converted to a string.
        public DateTime _dateTime;

        public DBResultData()
        {
            
            DBResults = new Dictionary<string, string>()
            {
                { "Variant_Id", null},
                { "Overall_Result", null},             
                { "Result1", null},
                { "Result2", null},
                { "Result3", null},
                { "Result4", null},
                { "Result5", null},
                { "Result6", null},
                { "Result7", null},
                { "Result8", null},
                { "Result9", null},
                { "Result10", null},
                { "Result11", null},
                { "Result12", null},
                { "FirstImage", null},
                { "SecondImage", null},
                { "ThirdImage", null},
            };
                      
        }
    }

    internal class PLCResults
    {
        public int _Header = 69;
        public Int16 _Result = 0;

        public PLCResults(bool ResultIn)
        {
            _Header = 69;
            _Result = Convert.ToInt16(ResultIn);
        }



        public byte[] DecodeSendData()
        {

            byte[] SendBytes = new byte[6];

            var tempbyte1 = DataStructureFunctions.SwapBytes(BitConverter.GetBytes(_Header));
            var tempbyte2 = DataStructureFunctions.SwapBytes(BitConverter.GetBytes(_Result));

            Array.Copy(tempbyte1, 0, SendBytes, 0, 4);
            Array.Copy(tempbyte2, 0, SendBytes, 4, 2);

            return SendBytes;
          
        }
    }

        
        
   
}
