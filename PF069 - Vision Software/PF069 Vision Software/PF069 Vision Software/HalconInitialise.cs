﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;
using System.Windows.Forms;
using NLog;

namespace HalconInterface
{
    static class HalconInitialise
    {

        private  static HDevEngine _Engine;
        private static HDevProgram _Program;
        private static HDevProgramCall _ProgramCall;
        private static string _ProgramPath = "";
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static HalconInitialise( )
        {
            _Engine = new HDevEngine();
            
        }

        public static bool LoadHalconProgram()
        {
            try
            {
                try
                {
                    _Program = new HDevProgram(_ProgramPath);
                    _ProgramCall = new HDevProgramCall(_Program);

                    return true;
                }

                catch (HDevEngineException Ex)
                {
                    logger.Error(Ex);
                    MessageBox.Show(Ex.Message, "HDevEngine Exception");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
        }

        public static HDevProgram Program
        {
            get { return _Program; }         
        }

        public static HDevEngine Engine
        {
            get { return _Engine; }         
        }
        public static string ProgramPath    
        {
            get { return _ProgramPath; }
            set { _ProgramPath = value; }
        }

    }
}
