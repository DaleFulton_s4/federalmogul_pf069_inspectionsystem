﻿namespace PF069_Vision_Software
{
    partial class frmResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControlResults = new DevExpress.XtraGrid.GridControl();
            this.resultsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCounter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVariantID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOverallResult = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResult1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResult2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResult3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResult5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColResult12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resultsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.resultsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlResults
            // 
            this.gridControlResults.DataSource = this.resultsBindingSource2;
            this.gridControlResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlResults.Location = new System.Drawing.Point(0, 0);
            this.gridControlResults.MainView = this.gridView1;
            this.gridControlResults.Name = "gridControlResults";
            this.gridControlResults.Size = new System.Drawing.Size(1100, 541);
            this.gridControlResults.TabIndex = 0;
            this.gridControlResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControlResults.Click += new System.EventHandler(this.gridControlResults_Click);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCounter,
            this.colVariantID,
            this.colTimeStamp,
            this.colOverallResult,
            this.colResult1,
            this.colResult2,
            this.colResult3,
            this.ColResult4,
            this.colResult5,
            this.ColResult6,
            this.ColResult7,
            this.ColResult8,
            this.ColResult9,
            this.ColResult10,
            this.ColResult11,
            this.ColResult12});
            this.gridView1.GridControl = this.gridControlResults;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colCounter
            // 
            this.colCounter.FieldName = "Counter";
            this.colCounter.Name = "colCounter";
            this.colCounter.Visible = true;
            this.colCounter.VisibleIndex = 0;
            this.colCounter.Width = 60;
            // 
            // colVariantID
            // 
            this.colVariantID.FieldName = "VariantID";
            this.colVariantID.Name = "colVariantID";
            this.colVariantID.Visible = true;
            this.colVariantID.VisibleIndex = 1;
            this.colVariantID.Width = 81;
            // 
            // colTimeStamp
            // 
            this.colTimeStamp.Caption = "Time Stamp";
            this.colTimeStamp.FieldName = "TimeStamp";
            this.colTimeStamp.Name = "colTimeStamp";
            this.colTimeStamp.Visible = true;
            this.colTimeStamp.VisibleIndex = 2;
            this.colTimeStamp.Width = 89;
            // 
            // colOverallResult
            // 
            this.colOverallResult.Caption = "Overall Result";
            this.colOverallResult.FieldName = "OverallResult";
            this.colOverallResult.Name = "colOverallResult";
            this.colOverallResult.Visible = true;
            this.colOverallResult.VisibleIndex = 3;
            this.colOverallResult.Width = 93;
            // 
            // colResult1
            // 
            this.colResult1.FieldName = "Result1";
            this.colResult1.Name = "colResult1";
            this.colResult1.Visible = true;
            this.colResult1.VisibleIndex = 4;
            this.colResult1.Width = 58;
            // 
            // colResult2
            // 
            this.colResult2.FieldName = "Result2";
            this.colResult2.Name = "colResult2";
            this.colResult2.Visible = true;
            this.colResult2.VisibleIndex = 5;
            this.colResult2.Width = 58;
            // 
            // colResult3
            // 
            this.colResult3.FieldName = "Result3";
            this.colResult3.Name = "colResult3";
            this.colResult3.Visible = true;
            this.colResult3.VisibleIndex = 6;
            this.colResult3.Width = 58;
            // 
            // ColResult4
            // 
            this.ColResult4.Caption = "Result4";
            this.ColResult4.FieldName = "Result4";
            this.ColResult4.Name = "ColResult4";
            this.ColResult4.Visible = true;
            this.ColResult4.VisibleIndex = 7;
            this.ColResult4.Width = 58;
            // 
            // colResult5
            // 
            this.colResult5.Caption = "Result5";
            this.colResult5.FieldName = "Result5";
            this.colResult5.Name = "colResult5";
            this.colResult5.Visible = true;
            this.colResult5.VisibleIndex = 8;
            this.colResult5.Width = 58;
            // 
            // ColResult6
            // 
            this.ColResult6.Caption = "Result6";
            this.ColResult6.FieldName = "Result6";
            this.ColResult6.Name = "ColResult6";
            this.ColResult6.Visible = true;
            this.ColResult6.VisibleIndex = 9;
            this.ColResult6.Width = 58;
            // 
            // ColResult7
            // 
            this.ColResult7.Caption = "Result7";
            this.ColResult7.FieldName = "Result7";
            this.ColResult7.Name = "ColResult7";
            this.ColResult7.Visible = true;
            this.ColResult7.VisibleIndex = 10;
            this.ColResult7.Width = 58;
            // 
            // ColResult8
            // 
            this.ColResult8.Caption = "Result8";
            this.ColResult8.FieldName = "Result8";
            this.ColResult8.Name = "ColResult8";
            this.ColResult8.Visible = true;
            this.ColResult8.VisibleIndex = 11;
            this.ColResult8.Width = 58;
            // 
            // ColResult9
            // 
            this.ColResult9.Caption = "Result9";
            this.ColResult9.FieldName = "Result9";
            this.ColResult9.Name = "ColResult9";
            this.ColResult9.Visible = true;
            this.ColResult9.VisibleIndex = 12;
            this.ColResult9.Width = 58;
            // 
            // ColResult10
            // 
            this.ColResult10.Caption = "Result10";
            this.ColResult10.FieldName = "Result10";
            this.ColResult10.Name = "ColResult10";
            this.ColResult10.Visible = true;
            this.ColResult10.VisibleIndex = 13;
            this.ColResult10.Width = 69;
            // 
            // ColResult11
            // 
            this.ColResult11.Caption = "Result11";
            this.ColResult11.FieldName = "Result11";
            this.ColResult11.Name = "ColResult11";
            this.ColResult11.Visible = true;
            this.ColResult11.VisibleIndex = 14;
            // 
            // ColResult12
            // 
            this.ColResult12.Caption = "Result12";
            this.ColResult12.FieldName = "Result12";
            this.ColResult12.Name = "ColResult12";
            this.ColResult12.Visible = true;
            this.ColResult12.VisibleIndex = 15;
            this.ColResult12.Width = 91;
            // 
            // frmResults
            // 
            this.ClientSize = new System.Drawing.Size(1100, 541);
            this.Controls.Add(this.gridControlResults);
            this.Name = "frmResults";
            this.Load += new System.EventHandler(this.frmResults_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource resultsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCounter;
        private DevExpress.XtraGrid.Columns.GridColumn colVariantID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStamp;
        private DevExpress.XtraGrid.Columns.GridColumn colOverallResult;
        private DevExpress.XtraGrid.Columns.GridColumn colResult1;
        private DevExpress.XtraGrid.Columns.GridColumn colResult2;
        private DevExpress.XtraGrid.Columns.GridColumn colResult3;
        private System.Windows.Forms.BindingSource resultsBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult4;
        private DevExpress.XtraGrid.Columns.GridColumn colResult5;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult6;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult7;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult8;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult9;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult10;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult11;
        private DevExpress.XtraGrid.Columns.GridColumn ColResult12;
        private System.Windows.Forms.BindingSource resultsBindingSource2;
    }
}