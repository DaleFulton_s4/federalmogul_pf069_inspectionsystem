﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using nsEventDistributer;
using nsEventTypes;
using Lib.TcpConnectionMonitor;
using System.Threading;
using NLog;



namespace TCPCommunication
{
    /* NOTES:
     * Base class contains an instance of a TcpConnectionMonitor as well as uses the static pinger class.
       The pinger class pings the TcpConnectionMonitor instance and verifies if a reply can be obtained. if so, the Isconnected property of the TcpConnectionMonitor is set to true.
       The Isconnected property is used in child classes to verify that the network cable has not been unplugged (pings receive replies).
     */

    public class BaseTCPComs
    {
        // This ip address is for the pinger to ping and in clients case, it is the ip to connect to.
        public string _IPAddress;
        public Int16 _Port;
        public TcpConnectionMonitor TcpConnection;
        public NetworkStream _NetworkStream;     
        public Queue<byte[]> DataSendQueue;
        public byte[] ReceivedBuffer = null;
        public delegate void RunMethodOnThread();
        public static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public object locker = new object();

        // call base class constructor
        public BaseTCPComs(string IPAddress, Int16 Port)
        {
            this._Port = Port;
            this._IPAddress = IPAddress;
            this.TcpConnection = new TcpConnectionMonitor(_IPAddress);
            Pinger.StartPinging(TcpConnection);
            
           
        }

        //Thread can only have one instance. If it is already alive, kill it and respawn it
        public void StartThread(ref Thread thread,  RunMethodOnThread Method)
        {

            if (thread == null || !thread.IsAlive)
            {
                try
                {             
                    thread = new Thread(() => Method());
                    thread.IsBackground = true;
                    thread.Start();               
                }
                catch(Exception ex)
                {
                    logger.Info("Start Thread Error: " + ex.ToString());
                }
            }
            else
            {
                try
                {
                    thread.Abort();
                }
                catch (ThreadAbortException)
                {
                    logger.Info(String.Format("TCP: Killed Thread: " + thread.Name.ToString() + " {0}:{1} ", _Port.ToString(), _IPAddress.ToString()));
                }
                finally
                {
                    thread = new Thread(() => Method());
                    thread.Start();
                }
            }
        }

        //Sends dynamic byte array over TCP. To send data, it is required to be placed in the DataSendQueue.
        public void SendByteArray()
        {
            var SendDequeued = DataSendQueue.Dequeue();
            _NetworkStream.Write(SendDequeued,0, SendDequeued.Length);
            SendDequeued = null; 
        }

        //Receive dynamic byte array over TCP
        public byte[] ReceiveByteArray(int size)
        {
            ReceivedBuffer = new byte[size];
            _NetworkStream.Read(ReceivedBuffer, 0, ReceivedBuffer.Length);
            return ReceivedBuffer;
        }





    }
}
