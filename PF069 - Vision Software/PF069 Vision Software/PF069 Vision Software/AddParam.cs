﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;

namespace PF069_Vision_Software
{
    public partial class frmAddParam : Form
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private ModelDBContext dbContext;
        public frmAddParam(ModelDBContext dbContext)
        {
            InitializeComponent();
            this.dbContext = dbContext;
        }

        private void AddParam_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
               
                Parameter Params = new Parameter
                {
                    VariantID = tbVarID.Text,
                    Gain = Convert.ToDouble(tbGain.Text),
                    Exposure = Convert.ToDouble(tbExposure.Text)
                };

                if (!dbContext.Parameters.Any(a => a.VariantID == Params.VariantID))
                {
                   
                    dbContext.Parameters.Add(Params);
                    Validate();
                    dbContext.SaveChanges();
                    MessageBox.Show(Form.ActiveForm, "Variant Successfully Added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(Form.ActiveForm, "Variant Already Exists", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.Close();            
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
                MessageBox.Show(Form.ActiveForm, "Error Occured", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
         
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
           
            this.Close();
        }
    }
    }
