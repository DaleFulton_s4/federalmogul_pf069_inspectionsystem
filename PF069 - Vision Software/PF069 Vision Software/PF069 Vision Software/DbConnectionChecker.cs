﻿using nsEventDistributer;
using nsEventTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PF069_Vision_Software;


namespace DatabaseConnection
{
    class DbConnectionChecker
    {
        private bool currentState = false;
        private bool newState = false;

        public DbConnectionChecker()
        {
            Run();
        }
        public void Run()
        {
            Thread thread = new Thread(Run_) { IsBackground = true };
            thread.Start();
        }

        private void Run_()
        {
            while (true)
            {
                newState = CheckConnection();
                if (newState != currentState)
                {
                    currentState = newState;
                    EventDistributer.Publish(new DbConnectionEvent(currentState));
                }
                Thread.Sleep(200);
            }
        }

        private bool CheckConnection()
        {
            using (var dbContext = new ModelDBContext())
            {
                try
                {
                    dbContext.Database.Connection.Open();
                    if (dbContext.Database.Connection.State == ConnectionState.Open)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
