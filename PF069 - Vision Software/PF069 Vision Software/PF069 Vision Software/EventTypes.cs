﻿using PF069_Vision_Software;

namespace nsEventTypes
{
    public enum EventType
    {
        coPacketReceived,
        coConnectionStatusChanged,
        coSocketConnected,
        coPacketSend,
        coSetTabFocus,
        coSetVisionStatus,
        coSetStatusStrip,
        coPublishDataResults,
        coDBStatusChanged
    }

    public abstract class BaseEvent
    {
        private EventType meType;

        public EventType Type
        {
            get { return meType;  }
            set { meType = value; }
        }
    }

    // Custom events
    // Events as classes as properties can send data 


    class TCPPacketReceivedEvt : BaseEvent
    {
        public byte[] Data { get; set; } = null;
        public TCPPacketReceivedEvt(byte[] Data)
        {
            Type = EventType.coPacketReceived;
            this.Data = Data;
        }
    }

    class ConnectionStatusChangedEvt : BaseEvent
    {
        public bool Status { get; set; } = false;
        public ConnectionStatusChangedEvt(bool Status)
        {
            Type = EventType.coConnectionStatusChanged;
            this.Status = Status;
        }
    }


    class TCPPacketSendEvt : BaseEvent
    {
        public byte[] Data { get; set; } = null;
        public TCPPacketSendEvt(byte[] Data)
        {
            Type = EventType.coPacketSend;
            this.Data = Data;
        }
    }

    class SetTabFocusEvt : BaseEvent
    {
        public int number { get; set; } 
        public SetTabFocusEvt(int number)
        {
            Type = EventType.coSetTabFocus;
            this.number = number;
        }
    }

    class SetVisionStatus: BaseEvent
    {
        public string message { get; set; }
        public SetVisionStatus(string message)
        {
            Type = EventType.coSetVisionStatus;
            this.message = message;
        }
    }

    class SetStatusStrip : BaseEvent
    {
        public string status { get; set; }
        public string colour { get; set; }
        public SetStatusStrip(string status, string colour)
        {
            Type = EventType.coSetStatusStrip;
            this.status = status;
            this.colour = colour;
        }
    }

    class PublishDBResults : BaseEvent
    {
        public DBResultData StructuredResults;
        public PublishDBResults(DBResultData results)
        {
            Type = EventType.coPublishDataResults;
            this.StructuredResults = results;
        }
    }

    class DbConnectionEvent : BaseEvent
    {
        public bool Status { get; set; } = false;
        public DbConnectionEvent(bool Status)
        {
            Type = EventType.coDBStatusChanged;
            this.Status = Status;
        }
    }

}
