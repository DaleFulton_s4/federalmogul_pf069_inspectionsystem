﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lib.TaskManager;
using NLog;
using HalconDotNet;
using HalconInterface;
using Lib.HalconProcedureInterface;
using lib.HalconUserControl;
using nsEventDistributer;
using nsEventTypes;
using System.Drawing;


namespace PF069_Vision_Software
{
    class VisionTask : BaseTask
    {
        public Int16 _Mode;
        public Int16 _Start;
        public Int16 _ProgramNo;
        public string _VariantID;
        public DateTime _DateTime;
        public HTuple _SaveImages;

        public bool OverallResult = false;
        public bool Result1 = false;
        public bool Result2 = false;
        public bool Result3 = false;
        public bool Result4 = false;
        public bool Result5 = false;
        public bool Result6 = false;
        public bool Result7 = false;
        public bool Result8 = false;
        public bool Result9 = false;
        public bool Result10 = false;
        public bool Result11 = false;
        public bool Result12 = false;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        //Window handle passed to this class (not using events) due to ensuring focus is set before executing vision task.
        HalconDisplayControl _halconDisplayControl;

        public VisionTask(Int16 Mode, Int16 Instruction, Int16 ProgramNo, lib.HalconUserControl.HalconDisplayControl halconDisplayControl, bool SaveImages)
        {
            _Mode = Mode;
            _Start = Instruction;
            _ProgramNo = ProgramNo;
            _halconDisplayControl = halconDisplayControl;
            // access from global as no other variants exist.
            _VariantID = ConfigGlobals.CurrentVariantID;
            _DateTime = DateTime.Now;
            _SaveImages = new HTuple(SaveImages); 
            
        }

        protected override void ExecuteImpl()
        {

            int NumberItems = 0;

            double[] AreaMin;
            double[] AreaMax;
            //Initialise dbContext
            ModelDBContext dBContext = new ModelDBContext();
            // Get Inspection Parameters

            // Due to poor table layout, some scores will not be used (they just read in null from DB)
            if (dBContext.Parameters.Any(a => a.VariantID == ConfigGlobals.CurrentVariantID))
            {
                Parameter GetParam = new Parameter();
                GetParam.Inspection = _ProgramNo;
                GetParam.Gain = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).Gain;
                GetParam.Exposure = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).Exposure;
                GetParam.AreaMin1 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMin1;
                GetParam.AreaMin2 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMin2;
                GetParam.AreaMin3 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMin3;
                GetParam.AreaMin4 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMin4;
                GetParam.AreaMin5 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMin5;
                GetParam.AreaMin6 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMin6;


                GetParam.AreaMax1 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMax1;
                GetParam.AreaMax2 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMax2;
                GetParam.AreaMax3 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMax3;
                GetParam.AreaMax4 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMax4;
                GetParam.AreaMax5 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMax5;
                GetParam.AreaMax6 = dBContext.Parameters.First(a => a.Inspection == GetParam.Inspection).AreaMax6;

                AreaMin = new double[6] { Convert.ToDouble(GetParam.AreaMin1), Convert.ToDouble(GetParam.AreaMin2), Convert.ToDouble(GetParam.AreaMin3), Convert.ToDouble(GetParam.AreaMin4), Convert.ToDouble(GetParam.AreaMin5), Convert.ToDouble(GetParam.AreaMin6) };
           
                AreaMax = new double[6] { Convert.ToDouble(GetParam.AreaMax1), Convert.ToDouble(GetParam.AreaMax2), Convert.ToDouble(GetParam.AreaMax3), Convert.ToDouble(GetParam.AreaMax4), Convert.ToDouble(GetParam.AreaMax5), Convert.ToDouble(GetParam.AreaMax6) };

            }
            else
            {
                logger.Info("Variant does not exist in the database");
                return;
            }

          

            EventDistributer.Publish(new SetVisionStatus("Busy inspecting..."));
            // Set Output Display
            _halconDisplayControl.SetHeadingText("Program: " + _ProgramNo.ToString());
            //dev set part called in halcon dynamically
            // _halconDisplayControl.GetWindow().SetPart(0, 0, Convert.ToInt32(ConfigGlobals._FrameGrabberList[0]._ImageHeight), Convert.ToInt32(ConfigGlobals._FrameGrabberList[0]._ImageWidth));
            HalconInitialise.Engine.SetHDevOperators(new HDevOpFixedWindowImpl(_halconDisplayControl.GetWindow()));

           

            switch (_ProgramNo)
            {
                case 1:
                    NumberItems = 6;
                    HalconProcedureInterface.Inspection1(NumberItems,AreaMin, AreaMax, ConfigGlobals._FrameGrabberList[0]._AcqHandle, _SaveImages, DataStructureFunctions.StringRemoveSpecialChar(_DateTime.ToString()), out OverallResult, out Result1, out Result2, out Result3, out Result4, out Result5, out Result6);
                 
                    break;
                case 2:
                    HalconProcedureInterface.Inspection2(ConfigGlobals._FrameGrabberList[0]._AcqHandle, out OverallResult, out Result7, out Result8, out Result9, out Result10, out Result11);
                  
                    break;
                case 3:
                    HalconProcedureInterface.Inspection3(ConfigGlobals._FrameGrabberList[0]._AcqHandle, out OverallResult, out Result12);
                    
                    break;
            }        

            if (OverallResult)
            {
                _halconDisplayControl.SetResultPass(Color.LightGreen, Color.Green);
            }
            else
            {
                _halconDisplayControl.SetResultFail(Color.Pink, Color.Red);
            }
            EventDistributer.Publish(new SetVisionStatus("Waiting for \n next inspection..."));



        }
    }
}
