﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PF069_Vision_Software
{
    public partial class frmLogin : Form
    {
        public bool Pass = false;

        public frmLogin()
        {
            InitializeComponent();
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if((textEditUsername.Text == "Admin") && (textEditPassword.Text == ConfigGlobals._AdminPassword))
            {
                Pass = true;
                this.Close();
                
            }
            else
            {
                MessageBox.Show(Form.ActiveForm, "Incorrect username or password", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
