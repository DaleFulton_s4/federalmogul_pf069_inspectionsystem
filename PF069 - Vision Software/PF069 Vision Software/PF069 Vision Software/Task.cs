﻿using NLog;
using System;
using System.Threading;

namespace Lib.TaskManager
{
    public abstract class BaseTask
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static long counter = 0;
        private Thread thread = null;

        protected abstract void ExecuteImpl();

        public long ID { get; }

        public event EventHandler TaskCompleted;
        public event EventHandler TaskAborted;
        public event EventHandler TaskFailed;

        public BaseTask()
        {
            counter++;
            ID = counter;
        }

        internal void Run()
        {
            thread = new Thread(() =>
            {
                try
                {
                    logger.Trace("Task " + ID.ToString() + ": STARTED");
                    ExecuteImpl();
                    logger.Trace("Task " + ID.ToString() + "Task Completed Successfully ");
                    // publish task compeleted event
                    TaskCompleted?.Invoke(this, new EventArgs());
                  
                }
                catch (ThreadAbortException e)
                {
                    logger.Error("Task " + ID.ToString() + ": ABORTED");
                    // publish task aborted event
                    TaskAborted?.Invoke(this, new EventArgs());
                }
                catch (Exception ex)
                {                
                    logger.Trace("Task " + ID.ToString() + "Task Completed with erros ");
                    logger.Error("Task " + ID.ToString() + ": " + ex);
                    // publish task failed event
                    TaskFailed?.Invoke(this, new EventArgs());
                }
            })
            {
                IsBackground = true
            };
            thread.Start();
        }

        public void Abort()
        {
            if (thread.IsAlive)
            {             
                thread.Abort();
                logger.Info("Current Task has been Aborted");
            }
        }

      

    }
}