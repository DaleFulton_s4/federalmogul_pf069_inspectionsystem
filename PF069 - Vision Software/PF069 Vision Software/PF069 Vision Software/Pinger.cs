﻿using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading;


namespace Lib.TcpConnectionMonitor
{
    public static class Pinger
    {
        private static readonly object lockObject = new object();
        private static List<string> tcpConnections = new List<string>();

        public static void StartPinging(TcpConnectionMonitor tcpConnection, int interval = 300, int timeout = 1000)
        {
            if (AddedConnection(tcpConnection))
            {
                Thread thread = new Thread(() =>
                {
                    Ping ping = new Ping();
                    while (tcpConnections.Contains(tcpConnection.Address))
                    {
                        try
                        {
                            PingReply reply = ping.Send(tcpConnection.Address, timeout);
                            tcpConnection.IsConnected = reply.Status == IPStatus.Success;
                        }
                        catch
                        {
                            tcpConnection.IsConnected = false;
                        }
                        Thread.Sleep(interval);
                    }
                })
                {
                    IsBackground = true
                };
                thread.Start();
            }
        }

        public static void StopPinging(TcpConnectionMonitor tcpConnection)
        {
            lock (lockObject)
            {
                while (tcpConnections.Contains(tcpConnection.Address))
                {
                    tcpConnections.Remove(tcpConnection.Address);
                }
            }
        }

        private static bool AddedConnection(TcpConnectionMonitor tcpConnection)
        {
            lock (lockObject)
            {
                if (!tcpConnections.Contains(tcpConnection.Address))
                {
                    tcpConnections.Add(tcpConnection.Address);
                    return true;
                }
                // could not add connection because a connection with the same IP already exists.
                return false;
            }
        }
    }
}