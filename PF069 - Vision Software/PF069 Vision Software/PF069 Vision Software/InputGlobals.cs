﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace PF069_Vision_Software
{
 

    public static class ConfigGlobals
    {
        public static string _ProgramPath { get; set; }
        // Total inspections refers to the number of images taken around the part, not the number of items to be inspected.
        public static int _TotalInspections;
        public static List<FrameGrabberStruct> _FrameGrabberList { get; set; }
        public static List<VariantStruct> _VariantStruct { get; set; }
        public static string _ThisPCIP { get; set; }
        public static Int16 _ThisPCPort { get; set; }
        public static string _PingerIP { get; set; }
        public static string _AdminPassword { get; set; }

        // This property is used to determine which variant is being tested by the system. It should be provided by PLC, but
        // since there is only one variant, it is hardcoded for now.
        public static string CurrentVariantID = "HS1";

        static ConfigGlobals()
        {
            _ProgramPath = "";
            _FrameGrabberList = new List<FrameGrabberStruct>();
            _VariantStruct = new List<VariantStruct>();
            _TotalInspections = 0;
            _AdminPassword = "";
        }
    }

    public class FrameGrabberStruct
    {
        public string _Interface { get; set; }
        public HTuple _AcqHandle { get; set; }
        public string _ColourSpace { get; set; }
        public string _CameraType { get; set; }
        public string _Device { get; set; }
        public string _ImageWidth { get; set; }
        public string _ImageHeight { get; set; }

    }

    public class VariantStruct
    {
        public string _ID { get; set; }
        public float _Exposure { get; set; }
        public float _Gain { get; set; }

    }
}
