﻿namespace PF069_Vision_Software
{
    partial class frmParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmParameters));
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanelParameters = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveChanges = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.gridControlParameters = new DevExpress.XtraGrid.GridControl();
            this.parametersBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVariantID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColInspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExposure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMin1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMin2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMin3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMin4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaMin5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMin6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMax1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.parametersBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.parametersBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.parametersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.parametersBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.parametersBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.fMVisionDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ColAreaMax2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMax3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMax4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMax5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAreaMax6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanelParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fMVisionDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.Location = new System.Drawing.Point(707, 516);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(170, 53);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete Entry";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.Location = new System.Drawing.Point(531, 516);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(170, 53);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add Entry";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tableLayoutPanelParameters
            // 
            this.tableLayoutPanelParameters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanelParameters.ColumnCount = 6;
            this.tableLayoutPanelParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelParameters.Controls.Add(this.btnDelete, 4, 1);
            this.tableLayoutPanelParameters.Controls.Add(this.btnAdd, 3, 1);
            this.tableLayoutPanelParameters.Controls.Add(this.btnSaveChanges, 5, 1);
            this.tableLayoutPanelParameters.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanelParameters.Controls.Add(this.gridControlParameters, 0, 0);
            this.tableLayoutPanelParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelParameters.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelParameters.Name = "tableLayoutPanelParameters";
            this.tableLayoutPanelParameters.RowCount = 2;
            this.tableLayoutPanelParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.88889F));
            this.tableLayoutPanelParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanelParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelParameters.Size = new System.Drawing.Size(1060, 578);
            this.tableLayoutPanelParameters.TabIndex = 0;
            this.tableLayoutPanelParameters.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanelParameters_Paint);
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnSaveChanges.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveChanges.ImageOptions.Image")));
            this.btnSaveChanges.Location = new System.Drawing.Point(883, 516);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(165, 53);
            this.btnSaveChanges.TabIndex = 4;
            this.btnSaveChanges.Text = "Save Changes";
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanelParameters.SetColumnSpan(this.label1, 3);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(3, 513);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(522, 65);
            this.label1.TabIndex = 5;
            this.label1.Text = "Press \"Save Changes\" to update the database";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // gridControlParameters
            // 
            this.tableLayoutPanelParameters.SetColumnSpan(this.gridControlParameters, 6);
            this.gridControlParameters.DataSource = this.parametersBindingSource5;
            this.gridControlParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlParameters.Location = new System.Drawing.Point(3, 3);
            this.gridControlParameters.MainView = this.gridView1;
            this.gridControlParameters.Name = "gridControlParameters";
            this.gridControlParameters.Size = new System.Drawing.Size(1054, 507);
            this.gridControlParameters.TabIndex = 6;
            this.gridControlParameters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControlParameters.Click += new System.EventHandler(this.gridControlParameters_Click_1);
            // 
            // parametersBindingSource5
            // 
            this.parametersBindingSource5.DataSource = typeof(PF069_Vision_Software.Parameter);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColID,
            this.colVariantID1,
            this.ColInspection,
            this.colExposure,
            this.colGain,
            this.ColAreaMin1,
            this.ColAreaMin2,
            this.ColAreaMin3,
            this.ColAreaMin4,
            this.colAreaMin5,
            this.ColAreaMin6,
            this.ColAreaMax1,
            this.ColAreaMax2,
            this.ColAreaMax3,
            this.ColAreaMax4,
            this.ColAreaMax5,
            this.ColAreaMax6});
            this.gridView1.GridControl = this.gridControlParameters;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            // 
            // ColID
            // 
            this.ColID.Caption = "ID";
            this.ColID.FieldName = "ID";
            this.ColID.Name = "ColID";
            this.ColID.Visible = true;
            this.ColID.VisibleIndex = 0;
            this.ColID.Width = 54;
            // 
            // colVariantID1
            // 
            this.colVariantID1.FieldName = "VariantID";
            this.colVariantID1.Name = "colVariantID1";
            this.colVariantID1.Visible = true;
            this.colVariantID1.VisibleIndex = 2;
            this.colVariantID1.Width = 86;
            // 
            // ColInspection
            // 
            this.ColInspection.Caption = "Inspection No.";
            this.ColInspection.FieldName = "Inspection";
            this.ColInspection.Name = "ColInspection";
            this.ColInspection.Visible = true;
            this.ColInspection.VisibleIndex = 1;
            this.ColInspection.Width = 106;
            // 
            // colExposure
            // 
            this.colExposure.FieldName = "Exposure";
            this.colExposure.Name = "colExposure";
            this.colExposure.Visible = true;
            this.colExposure.VisibleIndex = 3;
            this.colExposure.Width = 86;
            // 
            // colGain
            // 
            this.colGain.FieldName = "Gain";
            this.colGain.Name = "colGain";
            this.colGain.Visible = true;
            this.colGain.VisibleIndex = 4;
            this.colGain.Width = 55;
            // 
            // ColAreaMin1
            // 
            this.ColAreaMin1.Caption = "AreaMin 1";
            this.ColAreaMin1.FieldName = "AreaMin1";
            this.ColAreaMin1.Name = "ColAreaMin1";
            this.ColAreaMin1.Visible = true;
            this.ColAreaMin1.VisibleIndex = 5;
            this.ColAreaMin1.Width = 90;
            // 
            // ColAreaMin2
            // 
            this.ColAreaMin2.Caption = "AreaMin 2";
            this.ColAreaMin2.FieldName = "AreaMin2";
            this.ColAreaMin2.Name = "ColAreaMin2";
            this.ColAreaMin2.Visible = true;
            this.ColAreaMin2.VisibleIndex = 6;
            this.ColAreaMin2.Width = 90;
            // 
            // ColAreaMin3
            // 
            this.ColAreaMin3.Caption = "AreaMin 3";
            this.ColAreaMin3.FieldName = "AreaMin3";
            this.ColAreaMin3.Name = "ColAreaMin3";
            this.ColAreaMin3.Visible = true;
            this.ColAreaMin3.VisibleIndex = 7;
            this.ColAreaMin3.Width = 90;
            // 
            // ColAreaMin4
            // 
            this.ColAreaMin4.Caption = "AreaMin4";
            this.ColAreaMin4.FieldName = "AreaMin4";
            this.ColAreaMin4.Name = "ColAreaMin4";
            this.ColAreaMin4.Visible = true;
            this.ColAreaMin4.VisibleIndex = 8;
            this.ColAreaMin4.Width = 90;
            // 
            // colAreaMin5
            // 
            this.colAreaMin5.Caption = "AreaMin 5";
            this.colAreaMin5.FieldName = "AreaMin5";
            this.colAreaMin5.Name = "colAreaMin5";
            this.colAreaMin5.Visible = true;
            this.colAreaMin5.VisibleIndex = 9;
            this.colAreaMin5.Width = 90;
            // 
            // ColAreaMin6
            // 
            this.ColAreaMin6.Caption = "AreaMin 6";
            this.ColAreaMin6.FieldName = "AreaMin6";
            this.ColAreaMin6.Name = "ColAreaMin6";
            this.ColAreaMin6.Visible = true;
            this.ColAreaMin6.VisibleIndex = 10;
            this.ColAreaMin6.Width = 90;
            // 
            // ColAreaMax1
            // 
            this.ColAreaMax1.Caption = "AreaMax 1";
            this.ColAreaMax1.FieldName = "AreaMax1";
            this.ColAreaMax1.Name = "ColAreaMax1";
            this.ColAreaMax1.Visible = true;
            this.ColAreaMax1.VisibleIndex = 11;
            this.ColAreaMax1.Width = 107;
            // 
            // ColAreaMax2
            // 
            this.ColAreaMax2.Caption = "AreaMax 2";
            this.ColAreaMax2.FieldName = "AreaMax2";
            this.ColAreaMax2.Name = "ColAreaMax2";
            this.ColAreaMax2.Visible = true;
            this.ColAreaMax2.VisibleIndex = 12;
            // 
            // ColAreaMax3
            // 
            this.ColAreaMax3.Caption = "AreaMax3";
            this.ColAreaMax3.FieldName = "AreaMax3";
            this.ColAreaMax3.Name = "ColAreaMax3";
            this.ColAreaMax3.Visible = true;
            this.ColAreaMax3.VisibleIndex = 13;
            // 
            // ColAreaMax4
            // 
            this.ColAreaMax4.Caption = "AreaMax 4";
            this.ColAreaMax4.FieldName = "AreaMax4";
            this.ColAreaMax4.Name = "ColAreaMax4";
            this.ColAreaMax4.Visible = true;
            this.ColAreaMax4.VisibleIndex = 14;
            // 
            // ColAreaMax5
            // 
            this.ColAreaMax5.Caption = "AreaMax 5";
            this.ColAreaMax5.FieldName = "AreaMax5";
            this.ColAreaMax5.Name = "ColAreaMax5";
            this.ColAreaMax5.Visible = true;
            this.ColAreaMax5.VisibleIndex = 15;
            // 
            // ColAreaMax6
            // 
            this.ColAreaMax6.Caption = "AreaMax 6";
            this.ColAreaMax6.FieldName = "AreaMax6";
            this.ColAreaMax6.Name = "ColAreaMax6";
            this.ColAreaMax6.Visible = true;
            this.ColAreaMax6.VisibleIndex = 16;
            // 
            // frmParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 578);
            this.Controls.Add(this.tableLayoutPanelParameters);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmParameters";
            this.Text = "Parameters Table";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmParameters_FormClosing);
            this.Load += new System.EventHandler(this.frmParameters_Load);
            this.tableLayoutPanelParameters.ResumeLayout(false);
            this.tableLayoutPanelParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parametersBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fMVisionDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelParameters;
        private DevExpress.XtraEditors.SimpleButton btnSaveChanges;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.GridControl gridControlParameters;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource parametersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colVariantID1;
        private DevExpress.XtraGrid.Columns.GridColumn colExposure;
        private DevExpress.XtraGrid.Columns.GridColumn colGain;
        private System.Windows.Forms.BindingSource parametersBindingSource1;
        private System.Windows.Forms.BindingSource parametersBindingSource2;
        private System.Windows.Forms.BindingSource fMVisionDataSetBindingSource;

        private System.Windows.Forms.BindingSource parametersBindingSource3;
        private System.Windows.Forms.BindingSource parametersBindingSource4;
        private DevExpress.XtraGrid.Columns.GridColumn ColID;
        private DevExpress.XtraGrid.Columns.GridColumn ColInspection;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMin1;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMin2;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMin3;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMin4;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaMin5;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMin6;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMax1;
        private System.Windows.Forms.BindingSource parametersBindingSource5;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMax2;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMax3;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMax4;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMax5;
        private DevExpress.XtraGrid.Columns.GridColumn ColAreaMax6;
    }
}