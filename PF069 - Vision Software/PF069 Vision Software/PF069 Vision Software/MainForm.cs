﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;
using Lib.Xml;
using System.Data.Entity;
using DevExpress.XtraSplashScreen;
using System.Threading;
using Lib.TaskManager;
using HalconInterface;
using Lib.HalconProcedureInterface;
using nsEventTypes;
using HalconDotNet;
using nsEventDistributer;
using TCPCommunication.Server;
using lib.HalconUserControl;
using Lib.UtilityMethods;
using Lib.TcpConnectionMonitor;
using DatabaseConnection;

namespace PF069_Vision_Software
{
    public partial class MainForm : Form
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        // Use same instance of dbContext for adding results and displaying results on results form. 
        ModelDBContext dbContext = new ModelDBContext();
        frmParameters frmParameters = new frmParameters();
        frmResults frmResults;

        SplashScreenManager splashScreen;
        TaskManager TaskManager;
        ServerComs TCPServer;
        DbConnectionChecker DBchecker;

        private bool SaveImages = true;
        private bool SaveImagesStateChanged = true;
        private DBResultData NewData = null;

        TcpConnectionMonitor PLCConnection;


        public MainForm()
        {
            splashScreen = new SplashScreenManager(this, typeof(LoadingSplashScreen), true, true);
            InitializeComponent();
            //frmResults instantiated here so that dbcontext can be passed.
            frmResults = new frmResults(dbContext);

            //Subscribe Events
            EventDistributer.SubscribeForEvent(EventType.coPacketReceived, OnPacketReceived);
            EventDistributer.SubscribeForEvent(EventType.coSetTabFocus, OnSetTabFocus);
            EventDistributer.SubscribeForEvent(EventType.coSetStatusStrip, OnStatusUpdate);
            EventDistributer.SubscribeForEvent(EventType.coSetVisionStatus, OnVisionStatus);
            EventDistributer.SubscribeForEvent(EventType.coConnectionStatusChanged, OnConnectionStatusChanged);
            EventDistributer.SubscribeForEvent(EventType.coDBStatusChanged, OnDBConnectionChanged);

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            logger.Info("----Application Start----");
            bool AppStatus = false;

            //Run Task Manager on seperate thread
            TaskManager = TaskManager.Instance;
            // read inputs from config file
            AppStatus = SetFromConfigFile();
            //Set Halon Program Path
            HalconInitialise.ProgramPath = ConfigGlobals._ProgramPath;
            AppStatus = HalconInitialise.LoadHalconProgram();
            if (!AppStatus)
            {
                Application.Exit();
            }
            //Open all framegrabbers
            foreach (var item in ConfigGlobals._FrameGrabberList)
            {
                if (HalconProcedureInterface.FG_Open(item._Interface, item._ColourSpace, item._CameraType, item._Device) == false)
                {
                    logger.Trace("Could not open Framegrabber");
                    MessageBox.Show("Could not open Framegrabber");
                    Application.Exit();
                }
            }
            logger.Info("Saving of failed images has been turned on - State: " + SaveImages.ToString());
            TCPServer = new ServerComs(ConfigGlobals._ThisPCIP, ConfigGlobals._ThisPCPort, ConfigGlobals._PingerIP);
            PLCConnection = new TcpConnectionMonitor(ConfigGlobals._PingerIP);
            //EventDistributer.Publish(new SetStatusStrip("Startup successful: listening for client connection", "Pink"));
            DBchecker = new DbConnectionChecker();
        }



        private string ConvertResultBoolToString(bool Result)
        {
            if (Result == true)
            {
                return "PASS";
            }
            else
            {
                return "FAIL";
            }
        }

        // Obtains paramters from config file
        #region OBTAINING PARAMETERS


        // this method will read all variables from the config file and assign them to the global input variables
        private bool SetFromConfigFile()
        {
            try
            {
                // Read XML Config file
                XmlConfig Configfile = new XmlConfig("ConfigFile.xml");
                ConfigGlobals._TotalInspections = Convert.ToInt32(Configfile.ReadXmlSingleNode("/ConfigFile/HalconProgram/Inspections"));
                ConfigGlobals._ProgramPath = Configfile.ReadXmlSingleNode("/ConfigFile/HalconProgram/Path");
                ConfigGlobals._ThisPCPort = Convert.ToInt16(Configfile.ReadXmlSingleNode("/ConfigFile/Connections/ThisPCPort"));
                ConfigGlobals._ThisPCIP = Configfile.ReadXmlSingleNode("/ConfigFile/Connections/ThisPCIP");
                ConfigGlobals._PingerIP = Configfile.ReadXmlSingleNode("/ConfigFile/Connections/PingerIP/PLC");
                ConfigGlobals._AdminPassword = Configfile.ReadXmlSingleNode("/ConfigFile/AdminPassword");

                var tempFrameGrabbers = Configfile.ReadXmlChildNodes("/ConfigFile/FrameGrabbers");
    


                for (int i = 0; i < tempFrameGrabbers.Count; i++)
                {
                    ConfigGlobals._FrameGrabberList.Add(new FrameGrabberStruct());
                    ConfigGlobals._FrameGrabberList[i]._Interface = tempFrameGrabbers[i]["Interface"];
                    ConfigGlobals._FrameGrabberList[i]._ColourSpace = tempFrameGrabbers[i]["ColourSpace"];
                    ConfigGlobals._FrameGrabberList[i]._CameraType = tempFrameGrabbers[i]["CameraType"];
                    ConfigGlobals._FrameGrabberList[i]._Device = tempFrameGrabbers[i]["Device"];
                    ConfigGlobals._FrameGrabberList[i]._ImageWidth = tempFrameGrabbers[i]["ImageWidth"];
                    ConfigGlobals._FrameGrabberList[i]._ImageHeight = tempFrameGrabbers[i]["ImageHeight"];
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
        }

        #endregion


        #region UI
        private void windowsUIButtonPanel1_ButtonChecked(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button.Properties.Caption == "Status")
            {
                xtraTabControlSecondary.SelectedTabPage = xtraTabControlSecondary.TabPages[1];
            }

            if (e.Button.Properties.Caption == "Save Images")
            {
                SaveImages = true;
                if (SaveImagesStateChanged) { logger.Info("Saving of failed images has been turned on - State: " + SaveImages.ToString()); }
            }

        }

        private void windowsUIButtonPanel1_ButtonUnchecked(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button.Properties.Caption == "Status")
            {
                xtraTabControlSecondary.SelectedTabPage = xtraTabControlSecondary.TabPages[0];
            }
            if (e.Button.Properties.Caption == "Save Images")
            {
                if (MessageBox.Show("Are you sure you want to turn off Image Saving?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    frmLogin login = new frmLogin();
                    login.ShowDialog();
                    if (login.Pass == true)
                    {
                        SaveImages = false;
                        logger.Info("Saving of failed images has been turned off - State: " + SaveImages.ToString());
                        SaveImagesStateChanged = true;
                    }
                    else
                    {
                        SaveImagesStateChanged = false;
                        e.Button.Properties.Checked = true;
                    }
                }
                else
                {
                    SaveImagesStateChanged = false;
                    e.Button.Properties.Checked = true;
                }

            }
        }



        private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {

            switch (e.Button.Properties.Caption)
            {
                case "Results":
                    frmResults.ShowDialog();
                    break;
                case "Parameters":
                    frmLogin login = new frmLogin();
                    login.ShowDialog();
                    if (login.Pass == true)
                    { frmParameters.ShowDialog(); }
                    login.Dispose();
                    break;
            }

        }

        private void lblResult_Click(object sender, EventArgs e)
        {

        }

        private void xtraTabControlMain_Click(object sender, EventArgs e)
        {

        }
        #endregion

        // searches through the results dictionary and determines whether a key contains a null. If so, fills that keys value with "ABRT"
        public Dictionary<string, string> DictFillAbort(ref Dictionary<string, string> dict)
        {

            for (int index = 0; index < dict.Count; index++)
            {
                var item = dict.ElementAt(index);
                if (item.Value == null && item.Key.Contains("Result"))
                {
                    dict[item.Key] = "ABRT";
                }
            }
            return dict;
        }


        #region CALLBACK FUNCTIONS

        //PLC Status Changed
        public void OnDBConnectionChanged(BaseEvent Event)
        {
            DbConnectionEvent Args = (DbConnectionEvent)Event;
            if (Args.Status)
            {
                pnlDatabase.BackColor = Color.Green;
            }
            else
            {
                pnlDatabase.BackColor = Color.Red;

            }
        }
        //PLC Status Changed
        public void OnConnectionStatusChanged(BaseEvent Event)
        {
            ConnectionStatusChangedEvt Args = (ConnectionStatusChangedEvt)Event;
            if (Args.Status)
            {
                pnlPLCStatus.BackColor = Color.Green;
            }
            else
            {
                pnlPLCStatus.BackColor = Color.Red;
            }
        }

        //TCP data received from PLC
        public void OnPacketReceived(BaseEvent Event)
        {
            try
            {
                TCPPacketReceivedEvt Temp = (TCPPacketReceivedEvt)Event;
                DataStructure DS = new DataStructure(Temp.Data);
                HalconDisplayControl HDC = null;

                // ensure data structure is correct
                if (DS._FormatCorrect)
                {
                    // if start
                    if (DS._Start == 0)
                    {
                        this.InvokeIfRequired(() =>
                        {
                            lblProgram.Text = "Program: " + (DS._ProgramNo).ToString();
                            switch (DS._ProgramNo)
                            {
                                case 1:
                                    HDC = halconDisplayControl1;
                                    break;
                                case 2:
                                    HDC = halconDisplayControl2;
                                    break;
                                case 3:
                                    HDC = halconDisplayControl3;
                                    break;
                            }
                        });

                        VisionTask VT = new VisionTask(DS._Mode, DS._Start, DS._ProgramNo, HDC, SaveImages);

                        // tab change called here due to task manager timing.
                        EventDistributer.Publish(new SetTabFocusEvt(DS._ProgramNo));

                        //subscribe to task event.
                        VT.TaskCompleted += OnVisionTaskCompleted;
                        TaskManager.RunSequentially(VT);
                    }
                    else
                    {
                        //Abort
                        TaskManager.Clear();
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace(e);
            }
        }


        // This call back function is fired when a task is completed successfully in addition to the taskmanager call back function.
        public void OnVisionTaskCompleted(object sender, EventArgs e)
        {
            try
            {

                if ((sender is BaseTask task))
                {
                    task.TaskCompleted -= OnVisionTaskCompleted;

                    if (task is VisionTask VisionTask)
                    {
                        // Add results to datastructure if mode is not manual
                        // if machine is not in manual mode - manual mode only sends results to PLC
                        if (VisionTask._Mode != 0)
                        {

                            switch (VisionTask._ProgramNo)
                            {
                                case 1:
                                    NewData = new DBResultData();
                                    NewData.DBResults["Variant_Id"] = VisionTask._VariantID;
                                    NewData._dateTime = VisionTask._DateTime;

                                    NewData.DBResults["Result1"] = ConvertResultBoolToString(VisionTask.Result1);
                                    NewData.DBResults["Result2"] = ConvertResultBoolToString(VisionTask.Result2);
                                    NewData.DBResults["Result3"] = ConvertResultBoolToString(VisionTask.Result3);
                                    NewData.DBResults["Result4"] = ConvertResultBoolToString(VisionTask.Result4);
                                    NewData.DBResults["Result5"] = ConvertResultBoolToString(VisionTask.Result5);
                                    NewData.DBResults["Result6"] = ConvertResultBoolToString(VisionTask.Result6);

                                    if (!VisionTask.OverallResult && SaveImages)
                                    {
                                        NewData.DBResults["FirstImage"] = DataStructureFunctions.StringRemoveSpecialChar(VisionTask._DateTime.ToString());
                                    }

                                    break;

                                case 2:
                                    NewData.DBResults["Result7"] = ConvertResultBoolToString(VisionTask.Result7);
                                    NewData.DBResults["Result8"] = ConvertResultBoolToString(VisionTask.Result8);
                                    NewData.DBResults["Result9"] = ConvertResultBoolToString(VisionTask.Result9);
                                    NewData.DBResults["Result10"] = ConvertResultBoolToString(VisionTask.Result10);
                                    NewData.DBResults["Result11"] = ConvertResultBoolToString(VisionTask.Result11);

                                    if (!VisionTask.OverallResult && SaveImages)
                                    {
                                        NewData.DBResults["SecondImage"] = DataStructureFunctions.StringRemoveSpecialChar(VisionTask._DateTime.ToString());
                                    }

                                    break;

                                case 3:

                                    NewData.DBResults["Result12"] = ConvertResultBoolToString(VisionTask.Result12);

                                    if (!VisionTask.OverallResult && SaveImages)
                                    {
                                        NewData.DBResults["ThirdImage"] = DataStructureFunctions.StringRemoveSpecialChar(VisionTask._DateTime.ToString());
                                    }

                                    break;
                            }

                            //Overall result depends on the three vision task overall results.
                            if (NewData.DBResults["Overall_Result"] != "FAIL")
                            {
                                NewData.DBResults["Overall_Result"] = ConvertResultBoolToString(VisionTask.OverallResult);
                            }

                            if (!SaveImages)
                            {
                                NewData.DBResults["FirstImage"] = "IMG_OFF";
                                NewData.DBResults["SecondImage"] = "IMG_OFF";
                                NewData.DBResults["ThirdImage"] = "IMG_OFF";
                            }

                            //Full Cycle mode, only send results to DB once all tests are completed
                            if (VisionTask._Mode == 1)
                            {
                                if (ConfigGlobals._TotalInspections == VisionTask._ProgramNo)
                                {
                                    // subscribe to Event, then fire and unsubscribe
                                    EventDistributer.SubscribeForEvent(EventType.coPublishDataResults, OnPublishDBResults);
                                    EventDistributer.Publish(new PublishDBResults(NewData));
                                    EventDistributer.UnsubscribeFromEvent(EventType.coPublishDataResults, OnPublishDBResults);
                                    NewData = null;
                                }
                            }
                            else if (VisionTask._Mode == 2)
                            {
                                //Semi Cycle mode, send results to DB as soon as a failure occurs
                                if ((ConfigGlobals._TotalInspections == VisionTask._ProgramNo) || !VisionTask.OverallResult)
                                {
                                    // change all values that are null to "ABRT"
                                    DictFillAbort(ref NewData.DBResults);
                                    // subscribe to Event, then fire and unsubscribe
                                    EventDistributer.SubscribeForEvent(EventType.coPublishDataResults, OnPublishDBResults);
                                    EventDistributer.Publish(new PublishDBResults(NewData));
                                    EventDistributer.UnsubscribeFromEvent(EventType.coPublishDataResults, OnPublishDBResults);
                                    NewData = null;
                                }
                            }
                        }

                        // Send Results to PLC
                        PLCResults PlcResults = new PLCResults(VisionTask.OverallResult);
                        TCPServer.DataSendQueue.Enqueue(PlcResults.DecodeSendData());
                    }
                }
            }
            catch (NullReferenceException)
            {
                logger.Error(new NullReferenceException() + " System possibly out of sequence: Inpsections occured excluding Inspection 1");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public static byte[] SwapBytes(byte[] paBytes)
        {
            byte[] lobyte = paBytes;
            lobyte = lobyte.Reverse().ToArray();
            return lobyte;
        }




        public void OnPublishDBResults(BaseEvent Event)
        {
            try
            {
                PublishDBResults Args = (PublishDBResults)Event;

                this.InvokeIfRequired(() =>
                {

                    Result res = new Result
                    {
                        OverallResult = Args.StructuredResults.DBResults["Overall_Result"],
                        Result1 = Args.StructuredResults.DBResults["Result1"],
                        Result2 = Args.StructuredResults.DBResults["Result2"],
                        Result3 = Args.StructuredResults.DBResults["Result3"],
                        Result4 = Args.StructuredResults.DBResults["Result4"],
                        Result5 = Args.StructuredResults.DBResults["Result5"],
                        Result6 = Args.StructuredResults.DBResults["Result6"],
                        Result7 = Args.StructuredResults.DBResults["Result7"],
                        Result8 = Args.StructuredResults.DBResults["Result8"],
                        Result9 = Args.StructuredResults.DBResults["Result9"],
                        Result10 = Args.StructuredResults.DBResults["Result10"],
                        Result11 = Args.StructuredResults.DBResults["Result11"],
                        Result12 = Args.StructuredResults.DBResults["Result12"],
                        TimeStamp = Args.StructuredResults._dateTime,
                        VariantID = Args.StructuredResults.DBResults["Variant_Id"],
                        FirstImage = Args.StructuredResults.DBResults["FirstImage"],
                        SecondImage = Args.StructuredResults.DBResults["SecondImage"],
                        ThirdImage = Args.StructuredResults.DBResults["ThirdImage"],

                    };

                    dbContext.Results.Add(res);
                    dbContext.SaveChanges();
                    Validate();

                    logger.Info("Results have been published to the DB");

                });
            }
            catch (Exception e)
            {
                logger.Error(e);
            }

        }



        public void OnSetTabFocus(BaseEvent Event)
        {
            SetTabFocusEvt Args = (SetTabFocusEvt)Event;
            this.InvokeIfRequired(() =>
            {
                switch (Args.number)
                {
                    case 1:
                        xtraTabControlMain.SelectedTabPage = xtraTabControlMain.TabPages[0];
                        break;
                    case 2:
                        xtraTabControlMain.SelectedTabPage = xtraTabControlMain.TabPages[1];
                        break;
                    case 3:
                        xtraTabControlMain.SelectedTabPage = xtraTabControlMain.TabPages[2];
                        break;
                    case 4:
                        xtraTabControlMain.SelectedTabPage = xtraTabControlMain.TabPages[3];
                        break;
                }
            });
        }

        public void OnStatusUpdate(BaseEvent Event)
        {
            try
            {
                SetStatusStrip Args = (SetStatusStrip)Event;

                this.InvokeIfRequired(() =>
                {
                    StatusLabel1.Text = Args.status;
                    StatusLabel1.BackColor = Color.FromName(Args.colour);
                });
            }
            catch (Exception e)
            { logger.Error(e); }

        }

        public void OnVisionStatus(BaseEvent Event)
        {
            SetVisionStatus Args = (SetVisionStatus)Event;

            this.InvokeIfRequired(() =>
            {
                lblVisionStatus.Text = Args.message;
            });

        }




        #endregion

        private void lblCurrentInspection_Click(object sender, EventArgs e)
        {

        }

        private void windowsUIButtonPanel1_Click(object sender, EventArgs e)
        {

        }

        private void lblCamera_Click(object sender, EventArgs e)
        {

        }

        private void lblHeading_Click(object sender, EventArgs e)
        {

        }
    }
}
