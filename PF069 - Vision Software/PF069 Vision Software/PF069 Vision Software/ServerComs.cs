﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Windows.Forms;
using NLog;
using nsEventTypes;
using nsEventDistributer;
using System.Threading;
using Lib.TcpConnectionMonitor;

namespace TCPCommunication.Server
{
    public class ServerComs : BaseTCPComs
    {
        // IP Address of current machine you want to listen off
        private IPAddress _IpOwnAddress;
        private TcpListener _Listener;
        private TcpClient _Acceptedclient = null;

        private Thread Receiver = null;
        private Thread Listener = null;
        private Thread Sender = null;

        private bool RestartConnection = false;


        public ServerComs(string OwnIpAddress, Int16 Port, string ClientIp) : base(ClientIp, Port)
        {
            logger.Info(String.Format("TCP Connection Started - Connection Type: Server"));
            this._IpOwnAddress = IPAddress.Parse(OwnIpAddress);
            _Listener = new TcpListener(_IpOwnAddress, _Port);
            // starting connection for first time
            StartThread(ref Listener, StartListening);

        }

        // if method is a restart - first close and dispose of existing listeners
        private void StartListening()
        {
            EventDistributer.Publish(new SetStatusStrip("Listening for client connection ...", "Pink"));
            logger.Info(String.Format("TCP Thread Started: Listener Thread" + " {0}:{1} ", _Port.ToString(), _IpOwnAddress.ToString()));
            while (true)
            {
                try
                {
                    Thread.Sleep(1000);
                    if (TcpConnection.IsConnected)
                    {

                        if (!RestartConnection)
                        {
                            _Listener.Start();
                        }
                        else
                        {
                            // close all connections then restart listening
                            _Acceptedclient.Close();
                            _Acceptedclient.Dispose();

                            _Listener.Stop();
                            _Listener.Server.Close();
                            _Listener.Server.Dispose();

                            _Listener = new TcpListener(_IpOwnAddress, _Port);
                            _Listener.Start();
                        }

                        _Acceptedclient = new TcpClient();
                        _Acceptedclient = _Listener.AcceptTcpClient();
                        //Initialise Network Stream
                        _NetworkStream = _Acceptedclient.GetStream();
                        // Start Receiver thread
                        StartThread(ref Receiver, RunReceiver);

                        //Start Sender Thread               
                        StartThread(ref Sender, ServerSendData);
                        EventDistributer.Publish(new SetStatusStrip("Connected to client: " + _Port.ToString() + " : " + _IPAddress.ToString(), "White"));
                        EventDistributer.Publish(new SetVisionStatus("Waiting for \n next Inspection..."));
                        break;

                    }
                }
                catch(Exception ex)
                {
                    logger.Error("Listener Thread Error: " + ex.ToString());
                }
            }
        }

        public void RunReceiver()
        {
            
            logger.Info(String.Format("TCP Server Thread Started: Receiver Thread" + " {0}:{1} ", _Port.ToString(), _IPAddress.ToString()));
            // after first time runreceiver is called, connections count as restarts
            RestartConnection = true;
            while (true)
            {
                Thread.Sleep(100);

                if (TcpConnection.IsConnected)
                {
                    try
                    {

                        //poll server client connection to test if connection is still alive: https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.socket.poll?view=netframework-4.7.2true
                        var PolConnection = _Acceptedclient.Client.Poll(500, SelectMode.SelectRead);
                        if (PolConnection)
                        {
                            _NetworkStream = _Acceptedclient.GetStream();
                            //if data is availabe, read data else connection has been closed by the server so attempt to reconnect.
                            if (_NetworkStream.DataAvailable && _NetworkStream.CanRead)
                            {
                                logger.Trace(" Data Obtained from PLC " );
                                int BufferSize = _Acceptedclient.Client.Available;                      
                                EventDistributer.Publish(new TCPPacketReceivedEvt(ReceiveByteArray(BufferSize)));
                                
                            }
                            else
                            {
                                logger.Trace("Connection has been closed, reset or terminated");
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Info("Receiver Thread Error: " + ex.ToString());
                        break;
                    }
                }
                else
                {
                    logger.Error("IP Address cannot be pinged");
                    break;
                }
            }
            // restart listening
            StartThread(ref Listener, StartListening);
            
        }


        private void ServerSendData()
        {
            logger.Info(String.Format("TCP Server Thread Started: Sender Thread" + " {0}:{1} ", _Port.ToString(), _IPAddress.ToString()));

            // initialised here so that no data can be added to queue unless this thread is running. It will also reinitialise queue if thread respawns.
            DataSendQueue = new Queue<byte[]>();

            while (TcpConnection.IsConnected)
            {
                Thread.Sleep(100);
                try
                {
                    if (TcpConnection.IsConnected)
                    {
                        if (DataSendQueue.Count > 0)
                        {
                            SendByteArray();
                            logger.Trace("TCP Server: Data Sent");                           
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("Sender Thread Error: " + ex.ToString());
                }
            }

        }




    }



}







