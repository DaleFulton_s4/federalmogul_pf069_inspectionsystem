﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;

// NOTE: READXMLCHILDNOTES WILL ATTEMPT TO READ A COMMENT. PLACE COMMENT OUTSIDE OF DESIRED NODE TO BE READ.
// WHEN TIME ALLOWS, CHANGE METHODS TO IGNORE COMMENTS
namespace Lib.Xml
{
    class XmlConfig
    {
        private XmlDocument _XMLDoc;
        private XmlNode _Node;
        private XmlNodeList _NodeList;
        private string _DataRead;

        object locker = new object();

        // if file is within bin folder, only the name is required eg "testfile.xml"
        public XmlConfig(string FileName)
        {
            _XMLDoc = new XmlDocument();
            _DataRead = null;
            _Node = null;
            _NodeList = null;

            //ensure file exists

            _XMLDoc.Load(FileName);

        }

        // this method returns a dictionary for each node of name passed (returns a list). Each dictionary contains the name of the child node as well as the inner text of the child node
        public List<Dictionary<string, string>> ReadXmlChildNodes(string NodeName)
        {

                try
                {
                    // defines a list of all nodes with the desired name
                    _NodeList = _XMLDoc.SelectNodes(NodeName);
                    List<Dictionary<string, string>> List = new List<Dictionary<string, string>>();

                    for (int i = 0; i < _NodeList.Count; i++)
                    {
                        // create dictionary per element. Define dictionary for saving each child node name as key and inner text as element in dictionary.
                        Dictionary<string, string> NodesDict = new Dictionary<string, string>();

                        //check if it is an element i.e has a tag
                        if (_NodeList[i] is XmlElement)
                        {
                            //check that it has child nodes
                            if (_NodeList[i].HasChildNodes)
                            {
                                // create a list of all child nodes
                                XmlNodeList xmlNodeList = _NodeList[i].ChildNodes;

                                var tempChildElement = _NodeList[i];
                                var element = tempChildElement;

                                do
                                {

                                    // if element is not text and has no children
                                    if (!(element is XmlText && element.HasChildNodes == false))
                                    {
                                        element = element.FirstChild;
                                    }
                                    else
                                    {
                                        NodesDict.Add(element.ParentNode.Name, element.InnerText);
                                        element = element.ParentNode;

                                        while (element.NextSibling == null && element != tempChildElement)
                                        {
                                            element = element.ParentNode;
                                        }
                                        if (element != tempChildElement)
                                            element = element.NextSibling;

                                    }

                                } while (element != tempChildElement);

                            }

                        }

                        List.Add(NodesDict);
                    }

                    return List;

                }



            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Xml Read Exception");
                return null;
            }


        }

        // Reads inner text of a passed element (or node). must ensure node is of last level.
        public string ReadXmlSingleNode(string NodeName)
        {
            try
            {
                // Find the required node text
                _Node = _XMLDoc.SelectSingleNode(NodeName);
                // if _Node has an child i.e. has text.
                if (_Node.HasChildNodes) { _DataRead = _Node.InnerText; }
                return _DataRead;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Xml Read Exception");
                return null;
            }

        }
    }
}
